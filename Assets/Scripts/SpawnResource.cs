﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnResource : MonoBehaviour
{
    public Sprite[] resources;
    private Vector3 spot;
    public GameObject WaterPrefab, TweezerPrefab, Glovesprefab, Sawprefab, Splintprefab, Bandageprefab, Alcoholprefab, BurnGelprefab, Morphineprefab, Threadprefab;
    // Start is called before the first frame update
    void Start()
    {
        resources = new Sprite[11];
        for (int x = 1; x < 11; x++)
        {
            string num = x.ToString();
            string myPath = "Resources/" +"Sprites/Page" + num;
            Sprite toAdd = Resources.Load<Sprite>(myPath);
            resources[x] = toAdd;
            spot = new Vector3(7.22f, -17.31f, -15f);
        }
    }

    public void SpawnWater()
    {
        WaterPrefab.tag = "water";
        Instantiate(WaterPrefab, transform.position, Quaternion.identity);
    }
    public void SpawnTweezer()
    {
        TweezerPrefab.tag = "tweezer";
        Instantiate(TweezerPrefab, transform.position, Quaternion.identity);
    }
    public void SpawnGloves()
    {
        Glovesprefab.tag = "gloves";
        Instantiate(Glovesprefab, transform.position, Quaternion.identity);
    }
    public void SpawnSaw()
    {
        Sawprefab.tag = "saw";
        Instantiate(Sawprefab, transform.position, Quaternion.identity);
    }
    public void SpawnSplint()
    {
        Splintprefab.tag = "splint";
        Instantiate(Splintprefab, transform.position, Quaternion.identity);
    }
    public void SpawnBandage()
    {
        Bandageprefab.tag = "bandage";
        Instantiate(Bandageprefab, transform.position, Quaternion.identity);
    }
    public void SpawnAlcohol()
    {
        Alcoholprefab.tag = "alc";
        Instantiate(Alcoholprefab, transform.position, Quaternion.identity);
    }
    public void SpawnBurnGel()
    {
        BurnGelprefab.tag = "burngel";
        Instantiate(BurnGelprefab, transform.position, Quaternion.identity);
    }
    public void SpawnMorphine()
    {
        Morphineprefab.tag = "morphine";
        Instantiate(Morphineprefab, transform.position, Quaternion.identity);
    }
    public void SpawnThread()
    {
        Threadprefab.tag = "Thread";
        Instantiate(Threadprefab, transform.position, Quaternion.identity);
    }

   /* public void OnButtonPress()
    {
        if(this.name == "WaterB")
        {
            Instantiate(WaterPrefab, transform.position, Quaternion.identity);
        }
        else if(this.name == "TweezerB")
        {
            Instantiate(TweezerPrefab, transform.position, Quaternion.identity);
        }
        else if (this.name == "GlovesB")
        {
            Instantiate(Glovesprefab, transform.position, Quaternion.identity);
        }
        else if (this.name == "SawB")
        {
            Instantiate(Sawprefab, transform.position, Quaternion.identity);
        }
        else if (this.name == "SplintB")
        {
            Instantiate(Splintprefab, transform.position, Quaternion.identity);
        }
        else if (this.name == "BandageB")
        {
            Instantiate(Bandageprefab, transform.position, Quaternion.identity);
        }
        else if (this.name == "AlcoholB")
        {
            Instantiate(Alcoholprefab, transform.position, Quaternion.identity);
        }
        else if (this.name == "BurnGelB")
        {
            Instantiate(BurnGelprefab, transform.position, Quaternion.identity);
        }
        else if (this.name == "MorphineB")
        {
            Instantiate(Morphineprefab, transform.position, Quaternion.identity);
        }
        else if (this.name == "ThreadB")
        {
            Instantiate(Threadprefab, transform.position, Quaternion.identity);
        }
    }
    */
    // Update is called once per frame
    void Update()
    {
        
    }
}
