﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Handbook : MonoBehaviour
{
    public int sceneNum; // Which scene is this on?
    public int currentPage; // Which page does this currently show? This should by default be set to 1.
    public int maxPages; // How many pages are there?
    public Sprite[] bookPages; // Array of sprites of the pages.

    // Start is called before the first frame update
    public void Start()
    {
        bookPages = new Sprite[maxPages + 1];
        for (int x = 1; x < maxPages + 1; x++)
        {
            string num = x.ToString(); // Turn the variable into a string.

            // Find the correct path to the sprite.
            string myPath = "Scene" + sceneNum + "Sprites/Page" + num;

            // Make a sprite with the path.
            Sprite toAdd = Resources.Load<Sprite>(myPath);

            // Add it to the array of sprites.
            bookPages[x] = toAdd;
        }
        // By default the first page of the handbook is shown.
        this.GetComponent<SpriteRenderer>().sprite = bookPages[currentPage];
    }
}