﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PillCount : MonoBehaviour
{
    public Transform target;
    public Text pillCount;
    public Text applyResources;
    

    public GameObject painkillers;

    public bool isOver = false;
    public float pkTime = 1.5f;
    private int currentCount = 4;


    public float defaultPositionX;
    public float defaultPositionY;

    public bool removePk = false;

    public GameObject HP;
    public Camera mCamera;

    private void Start()
    {
        painkillers = GameObject.FindGameObjectWithTag("painkill");
        HP = GameObject.FindGameObjectWithTag("HealthBar");
        mCamera = Camera.main;
        //defaultPositionX = mCamera.WorldToScreenPoint(target.position);
       //defaultPositionY = mCamera.WorldToScreenPoint(target.position);
        defaultPositionX = 5.0613f;
        defaultPositionY = -18.21281f;

    }


    // Update is called once per frame
    void Update()
    {

        //x bounds for head = 0.9 - 2.0
        //x bounds for body = 2.5 - 4.3

        pillCount.text = "x " + currentCount;
        if(isOver == true)
        {
            if (Input.GetKey("e") && currentCount != 0 && pkTime > 0)
            {
                pkTime -= Time.deltaTime;
                applyResources.text = "Applying Painkillers\n" + pkTime.ToString("F2");

                if (pkTime <= 0)
                {
                    HP.GetComponent<HealthBar>().newHealth += 10f;
                    //HP.GetComponent<HealthBar>().healthdecrease -= .0002f;
                    applyResources.text = "";
                    isOver = false;
                    removePk = true;
                }
            }
        }
        if(isOver == false)
        {
            painkillers.transform.localPosition = new Vector3(defaultPositionX, defaultPositionY, 0);
            pkTime = 1.5f;

        }
        /*if (painkillers.transform.localPosition.x > 0.9 && painkillers.transform.localPosition.x < 2.0)
        {


            if (Input.GetKey("e") && currentCount != 0 && pkTime > 0)
            {
                pkTime -= 0.01f;
                applyResources.text = "Applying Painkillers\n" + pkTime.ToString("F2");

                if (pkTime <= 0)
                {
                    HP.GetComponent<HealthBar>().newHealth += .2f;
                    HP.GetComponent<HealthBar>().healthdecrease -= .0002f;
                    applyResources.text = "";
                    removePk = true;
                }

            }
        }*/

        if(removePk == true)
        {
            applyResources.text = "";
            currentCount -= 1;
            removePk = false;
            pkTime = 1.5f; //default value
                           //snap that shit back to the starting position
            //painkillers.transform.localPosition = new Vector3(-3.89f, -3.33f, 0);
            painkillers.transform.localPosition = new Vector3(defaultPositionX, defaultPositionY, 0);
        }


        if (currentCount == 0)
        {
            painkillers.transform.gameObject.SetActive(false);
        }

        
    }
}
