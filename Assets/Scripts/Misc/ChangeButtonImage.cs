﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeButtonImage : MonoBehaviour

{

    public Button TreatmentButton;
    public Sprite neutral;
    public Sprite hover;
    public GameObject neutralButton;
    public GameObject hoverButton;
    public SpriteRenderer neutralSprite;
    public SpriteRenderer hoverSprite;

    // Start is called before the first frame update
    void Start()
    {
        neutralButton = GameObject.Find("bioFinishButtonNeutral");
        hoverButton = GameObject.Find("bioFinishButtonHover");
        neutralSprite = neutralButton.GetComponent<SpriteRenderer>();
        hoverSprite = hoverButton.GetComponent<SpriteRenderer>();
        neutral = neutralSprite.sprite;
        hover = hoverSprite.sprite;

    }

    public void NeutralImage()
    {
        TreatmentButton.GetComponent<Image>().sprite = neutral;
    }

    public void HoverImage()
    {
        TreatmentButton.GetComponent<Image>().sprite = hover;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
