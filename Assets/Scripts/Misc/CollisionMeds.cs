﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionMeds : MonoBehaviour
{
    public GameObject painkillers;
    void Start()
    {
        painkillers = GameObject.FindGameObjectWithTag("painkillUI");
    }
    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.tag == "giveBandages")
        {
            Physics2D.IgnoreCollision(painkillers.GetComponent<Collider2D>(), painkillers.GetComponent<Collider2D>());
        }
        if (col.gameObject.tag == "giveMeds")
        {
            Debug.Log("Collision");
            painkillers.GetComponent<PillCount>().isOver = true;
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "giveBandages")
        {
            Physics2D.IgnoreCollision(painkillers.GetComponent<Collider2D>(), painkillers.GetComponent<Collider2D>());
        }
        if (col.gameObject.tag == "giveMeds")
        {
            Debug.Log("Exit");
            painkillers.GetComponent<PillCount>().isOver = false;
        }
    }
 }
