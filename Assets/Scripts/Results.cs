﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Results : MonoBehaviour
{
    public GameObject Patient1;
    public GameObject Patient2;
    public GameObject Patient3;
    public GameObject Patient4;

    public GameObject bandageAdd;
    public GameObject waterAdd;
    public GameObject alcAdd;
    public GameObject splintAdd;
    public GameObject burngelAdd;
    public GameObject threadAdd;
    public GameObject morphineAdd;
    public GameObject savedText;

    public GameObject bandageNum;
    public GameObject waterNum;
    public GameObject alcNum;
    public GameObject splintNum;
    public GameObject burngelNum;
    public GameObject threadNum;
    public GameObject morphineNum;

    public GameObject fadeOut;

    private int bandageIncome = 1;
    private int alcIncome = 1;
    private int waterIncome = 1;
    private int threadIncome = 1;
    private int splintIncome = 1;
    private int burngelIncome = 1;
    private int morphineIncome = 1;
    private int saved = 0;
    // Start is called before the first frame update
    void Start()
    {
        fadeOut.GetComponent<FadeOut>().startFading();
        if (ResourceCount.Patient1Status == false)
        {
            Patient1.SetActive(false);
        }
        else if (ResourceCount.Patient1Status == true)
        {
            int random1 = Random.Range(1, 2);
            int random2 = Random.Range(1, 3);
            ResourceCount.bandageCount += random1; 
            ResourceCount.burngelCount += random2;

            bandageIncome += random1;
            burngelIncome += random2;
            saved += 1;
        }



        if (ResourceCount.Patient2Status == false)
        {
            Patient2.SetActive(false);
        }
        else if (ResourceCount.Patient1Status == true)
        {
            int random1 = Random.Range(1, 3); ;
            int random2 = Random.Range(2, 4); ;
            int random3 = 1;
            ResourceCount.waterCount += random1; 
            ResourceCount.alcCount += random2; 
            ResourceCount.threadCount += random3;

            waterIncome += random1;
            alcIncome += random2;
            threadIncome += 1;
            saved += 1;
        }



        if (ResourceCount.Patient3Status == false)
        {
            Patient3.SetActive(false);
        }
        else if (ResourceCount.Patient1Status == true)
        {
            int random1 = Random.Range(1, 2); 
            int random2 = 1;

            ResourceCount.waterCount += Random.Range(1, 2);
            ResourceCount.morphineCount += random2;

            waterIncome += random1;
            morphineIncome += random2;
            saved += 1;
        }



        if (ResourceCount.Patient4Status == false)
        {
            Patient4.SetActive(false);
        }
        else if (ResourceCount.Patient1Status == true)
        {
            int random1 = Random.Range(1, 2); 
            int random2 = Random.Range(1, 2);
            int random3 = Random.Range(1, 2);

            ResourceCount.burngelCount += random1; 
            ResourceCount.alcCount += random2; 
            ResourceCount.waterCount += random3;

            burngelIncome += random1;
            alcIncome += random2;
            waterIncome += random3;
            saved += 1;
        }

        bandageAdd.GetComponent<Text>().text = " + " + bandageIncome.ToString();
        waterAdd.GetComponent<Text>().text = " + " + waterIncome.ToString();
        alcAdd.GetComponent<Text>().text = " + " + alcIncome.ToString();
        threadAdd.GetComponent<Text>().text = " + " + threadIncome.ToString();
        splintAdd.GetComponent<Text>().text = " + " + splintIncome.ToString();
        burngelAdd.GetComponent<Text>().text = " + " + burngelIncome.ToString();
        morphineAdd.GetComponent<Text>().text = " + " + morphineIncome.ToString();

        savedText.GetComponent<Text>().text = "Patients Treated: " + saved.ToString();

          bandageNum.GetComponent<Text>().text = " Total: " + ResourceCount.bandageCount.ToString();
            waterNum.GetComponent<Text>().text = " Total: " + ResourceCount.waterCount.ToString();
            alcNum.GetComponent<Text>().text = " Total: " + ResourceCount.alcCount.ToString();
            threadNum.GetComponent<Text>().text = " Total: " + ResourceCount.threadCount.ToString();
            splintNum.GetComponent<Text>().text = " Total: " + ResourceCount.splintCount.ToString();
            burngelNum.GetComponent<Text>().text = " Total: " + ResourceCount.burngelCount.ToString();
            morphineNum.GetComponent<Text>().text = " Total: " + ResourceCount.morphineCount.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
