﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scrapes : MonoBehaviour
{
    public bool cleaned;
    private bool bandaged;
    private float applyTimer;

    public bool alcOver = false;
    public bool bandageOver = false;
    private bool switches = false;

    public GameObject H3;
    public GameObject change;
    public GameObject current;
    public GameObject global;
    public GameObject Loadbar;
    public GameObject eButton;

    public Transform applyBar;

    private string currentName;
    public string bodyPart;

    public AudioSource tape;
    public AudioSource open;
    private bool playingOpen = false;
    private bool applyProcess = false;
    // Start is called before the first frame update
    void Start()
    {
        bandaged = false;
        cleaned = false;
        currentName = current.gameObject.name;
        if(currentName == "soldier3cutLowerRightLeg")
        {
            bodyPart = "leg";
        }
        else if(currentName == "soldier3cutLowerRightArm")
        {
            bodyPart = "arm";
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (cleaned == false && alcOver == true && ResourceCount.alcCount > 0)
        {
            if(applyProcess == false)
            {
                eButton.GetComponent<SpriteRenderer>().enabled = true;
            }
            if (Input.GetKey("e"))
            {
                if (applyProcess == false)
                {
                    eButton.GetComponent<SpriteRenderer>().enabled = false;
                    applyTimer = 1.0f;
                    applyProcess = true;
                }
                Loadbar.SetActive(true);
                applyTimer -= Time.deltaTime;
                if (playingOpen == false)
                {
                    open.Play();
                    playingOpen = true;
                }

                applyBar.localScale = new Vector3(applyTimer, 1f);
                if (applyTimer <= 0)
                {
                    ResourceCount.alcCount -= 1;
                    Debug.Log("CompleteAlc");
                    cleaned = true;
                    Loadbar.SetActive(false);
                    applyTimer = 1.0f;
                    applyProcess = false;
                }
            }
            else if (Input.GetKeyUp("e") || alcOver == false)
            {
                applyTimer = 1.0f;
                playingOpen = false;
                Loadbar.SetActive(false);
                applyProcess = false;
            }
        }
        else if(cleaned == false && alcOver == false)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }


        if (bandaged == false && bandageOver == true && cleaned == true && ResourceCount.bandageCount > 0)
        {
            if(applyProcess == false)
            {
                eButton.GetComponent<SpriteRenderer>().enabled = true;
            }
            if (Input.GetKey("e"))
            {
                if (applyProcess == false)
                {
                    eButton.GetComponent<SpriteRenderer>().enabled = false;
                    applyTimer = 2.0f;
                    applyProcess = true;
                }
                Loadbar.SetActive(true);
                applyTimer -= Time.deltaTime;
                applyBar.localScale = new Vector3(applyTimer / 2.0f, 1f);
                if (applyTimer <= 0)
                {
                    tape.Play();
                    ResourceCount.bandageCount -= 1;
                    Debug.Log("Bandages Left:" + ResourceCount.bandageCount);
                    Loadbar.SetActive(false);
                    bandaged = true;
                    current.SetActive(false);
                    change.SetActive(true);
                    applyTimer = 0f;
                    applyProcess = false;
                }
            }
            else if (Input.GetKeyUp("e") || bandageOver == false)
            {
                applyTimer = 0f;
                Loadbar.SetActive(false);
                applyProcess = false;
            }
        }
        else if(bandageOver == false && bandaged == false && cleaned == true)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }

        if(bandaged == true && switches == false && bodyPart == "arm")
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
            switches = true;
            H3.GetComponent<HealthBar3>().arm = true;
        }
        if (bandaged == true && switches == false && bodyPart == "leg")
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
            switches = true;
            H3.GetComponent<HealthBar3>().leg = true;
        }

        if(applyProcess == true)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }
    }


}
