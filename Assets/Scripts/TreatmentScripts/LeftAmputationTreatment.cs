﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftAmputationTreatment : MonoBehaviour
{
    private bool cleaned;
    private bool hand;
    private bool bandaged;
    private float applyTimer;


    public bool handOver = false;
    public bool alcOver = false;
    public bool bandageOver = false;

    public GameObject change;
    public GameObject H2;
    public GameObject target;
    public GameObject global;
    public GameObject Loadbar;
    public GameObject eButton;

    public Transform applyBar;

    public AudioSource tape;
    public AudioSource open;
    private bool playingOpen = false;
    private bool applyProcess = false;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (hand == false && handOver == true)
        {
            if(applyProcess == false)
            {
                eButton.GetComponent<SpriteRenderer>().enabled = true;
            }

            if (Input.GetKey("e"))
            {
                if (applyProcess == false)
                {
                    eButton.GetComponent<SpriteRenderer>().enabled = false;
                    applyTimer = 1.0f;
                    applyProcess = true;
                }
                Loadbar.SetActive(true);
                applyTimer -= Time.deltaTime;
                applyBar.localScale = new Vector3(applyTimer, 1f);
                if (applyTimer <= 0)
                {
                    Debug.Log("CompleteHand");
                    hand = true;
                    applyTimer = 1.0f;
                    Loadbar.SetActive(false);
                    applyProcess = false;
                }
            }
            else if (Input.GetKeyUp("e") || handOver == false)
            {
                applyTimer = 1.0f;
                Loadbar.SetActive(false);
                applyProcess = false;
            }
        }
        else if(handOver == false && hand == false)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }


        if (cleaned == false && alcOver == true && ResourceCount.alcCount > 0 && hand == true)
        {
            if (applyProcess == false)
            {
                eButton.GetComponent<SpriteRenderer>().enabled = true;
            }
            if (Input.GetKey("e"))
            {

                if (applyProcess == false)
                {
                    eButton.GetComponent<SpriteRenderer>().enabled = false;
                    applyTimer = 3.0f;
                    applyProcess = true;
                }
                Loadbar.SetActive(true);
                applyTimer -= Time.deltaTime;
                if (playingOpen == false)
                {
                    open.Play();
                    playingOpen = true;
                }
                applyBar.localScale = new Vector3(applyTimer / 3.0f, 1f);
                if (applyTimer <= 0)
                {
                    Debug.Log("CompleteAlc");
                    ResourceCount.alcCount -= 1;
                    cleaned = true;
                    applyTimer = 3.0f;
                    Loadbar.SetActive(false);
                    applyProcess = false;
                }
            }
            else if (Input.GetKeyUp("e") || alcOver == false)
            {
                applyTimer = 3.0f;
                Loadbar.SetActive(false);
                playingOpen = false;
                applyProcess = false;
            }
        }
        else if(alcOver == false && hand == true && cleaned == false)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }



        if (bandaged == false && bandageOver == true && ResourceCount.bandageCount > 0 && cleaned == true && hand == true)
        {
            if (applyProcess == false)
            {
                eButton.GetComponent<SpriteRenderer>().enabled = true;
            }
            if (Input.GetKey("e"))
            {
                if (applyProcess == false)
                {
                    applyTimer = 5.0f;
                    applyProcess = true;
                }
                Loadbar.SetActive(true);
                applyTimer -= Time.deltaTime;
                applyBar.localScale = new Vector3(applyTimer / 5.0f, 1f);
                if (applyTimer <= 0)
                {
                    tape.Play();
                    ResourceCount.bandageCount -= 1;
                    Debug.Log("CompleteBandage");
                    target.SetActive(false);
                    change.SetActive(true);
                    bandaged = true;
                    applyTimer = 1.0f;
                    Loadbar.SetActive(false);
                    applyProcess = false;
                }
            }
            else if (Input.GetKeyUp("e") || bandageOver == false)
            {
                applyTimer = 5.0f;
                Loadbar.SetActive(false);
                applyProcess = false;               
            }
        }
        else if(bandageOver == false && hand == true && cleaned == true && bandaged == false)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }

        if (bandaged == true && hand == true && cleaned == true)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
            H2.GetComponent<HealthBar2>().arm = true;
            //set Treated == true;
            //set Done == true;
        }
        if (applyProcess == true)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }
    }
}
