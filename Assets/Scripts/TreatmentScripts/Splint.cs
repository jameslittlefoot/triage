﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splint : MonoBehaviour
{
    // Start is called before the first frame update
    private bool hand;
    private bool splinted;
    public bool handOver = false;
    public bool splintOver = false;

    private float applyTimer = 1.0f;
    private float applyTimer2 = 3.0f;
    private bool switches = false;

    public GameObject H3;
    public GameObject change;
    public GameObject current;
    public GameObject global;
    public GameObject Loadbar;
    public GameObject eButton;

    public Transform applyBar;

    public AudioSource tape;

    private string currentName;
    public string bodyPart;
    private bool applyProcess;
    // Start is called before the first frame update
    void Start()
    {
        splinted = false;
        currentName = current.gameObject.name;
        if (currentName == "soldier4BrokenLowerRightArm")
        {
            bodyPart = "arm";
        }
        else
        {
            bodyPart = "torso";
        }
    }

    // Update is called once per frame
    void Update()
    {
        if ( hand == false && handOver == true)
        {
            if(applyProcess == false)
            {
                eButton.GetComponent<SpriteRenderer>().enabled = true;
            }
            if (Input.GetKey("e"))
            {
                if(applyProcess == false)
                {
                    eButton.GetComponent<SpriteRenderer>().enabled = false;
                    applyProcess = true;
                }
                Loadbar.SetActive(true);
                applyTimer -= Time.deltaTime;
                applyBar.localScale = new Vector3(applyTimer, 1f);
                if (applyTimer <= 0)
                {
                    applyProcess = false;
                    hand = true;
                    Loadbar.SetActive(false);
                    applyTimer = 1.0f;
                }
            }
            else if (Input.GetKeyUp("e") || handOver == false)
            {
                applyTimer = 1.0f;
                Loadbar.SetActive(false);
                applyProcess = false;
            }
        }
        else if (handOver == false && hand == false)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }



        if (splinted == false && splintOver == true && ResourceCount.splintCount > 0 && hand == true)
        {
            if(applyProcess == false)
            {
                eButton.GetComponent<SpriteRenderer>().enabled = true;
            }
            if (Input.GetKey("e"))
            {
                if(applyProcess == false)
                {
                    eButton.GetComponent<SpriteRenderer>().enabled = false;
                    applyProcess = true;
                }
                Loadbar.SetActive(true);
                applyTimer2 -= Time.deltaTime;
                applyBar.localScale = new Vector3(applyTimer2 / 3.0f, 1f);
                applyTimer2 -= Time.deltaTime;
                if (applyTimer2 <= 0)
                {
                    applyProcess = false;
                    Loadbar.SetActive(false);
                    tape.Play();
                    ResourceCount.splintCount -= 1;
                    splinted = true;
                    applyTimer2 = 3.0f;
                    current.SetActive(false);

                }
            }
            else if (Input.GetKeyUp("e") || splintOver == false)
            {
                applyTimer2 = 3.0f;
                Loadbar.SetActive(false);
                applyProcess = false;
            }
        }
        else if (splintOver == false && splinted == false && hand == true)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }


        if (splinted == true && switches == false && bodyPart == "arm")
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
            switches = true;
            change.SetActive(true);
            H3.GetComponent<HealthBar4>().arm = true;
        }
        if (splinted == true && switches == false && bodyPart == "torso")
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
            switches = true;
            change.SetActive(true);
            H3.GetComponent<HealthBar3>().torso = true;
        }

        if (applyProcess == true)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }


    }
}
