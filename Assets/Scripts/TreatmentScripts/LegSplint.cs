﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegSplint : MonoBehaviour
{
    private bool splinted;
    private float applyTimer = 2.5f;
    public bool splintOver = false;

    public GameObject H2;
    public GameObject change;
    public GameObject target;
    public GameObject global;
    public GameObject Loadbar;
    public GameObject eButton;
    public Transform applyBar;

    public AudioSource tape;
    private bool applyProcess = false;
    // Start is called before the first frame update
    void Start()
    {
        splinted = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (splinted == false && splintOver == true && ResourceCount.splintCount > 0)
        {
            if (applyProcess == false)
            {
                eButton.GetComponent<SpriteRenderer>().enabled = true;
            }
            if (Input.GetKey("e"))
            {
                if(applyProcess == false)
                {
                    eButton.GetComponent<SpriteRenderer>().enabled = false;
                    applyProcess = true;
                }
                Loadbar.SetActive(true);
                applyTimer -= Time.deltaTime;
                applyBar.localScale = new Vector3(applyTimer / 2.5f, 1f);
                applyTimer -= Time.deltaTime;
                if (applyTimer <= 0)
                {
                    applyProcess = false;
                    Loadbar.SetActive(false);
                    tape.Play();
                    ResourceCount.splintCount -= 1;
                    splinted = true;
                    applyTimer = 2.5f;
                    target.SetActive(false);
                    change.SetActive(true);
                    H2.GetComponent<HealthBar2>().leg = true;
                }
            }
            else if (Input.GetKeyUp("e") || splintOver == false)
            {
                applyTimer = 2.5f;
                Loadbar.SetActive(false);
                applyProcess = false;
            }
        }
        else if(splinted == false && splintOver == false)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }

        if (applyProcess == true)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }

    }
}
