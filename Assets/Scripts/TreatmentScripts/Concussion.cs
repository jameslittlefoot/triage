﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Concussion : MonoBehaviour
{
    private bool bandaged;
    private bool hand;
    private bool water;
    private GameObject thigh;
    private float applyTimer = 1.0f;
    public bool handOver = false;
    public bool bandageOver = false;
    public bool waterOver = false;
    public bool switches = false;


    public GameObject H4;
    public GameObject change;
    public GameObject current;
    public GameObject global;
    public GameObject Loadbar;
    public GameObject eButton;

    public AudioSource tape;
    public AudioSource breathing;
    public Transform applyBar;
    private bool applyProcess = false;
    // Start is called before the first frame update
    void Start()
    {
        bandaged = false;
        hand = false;
        water = false;
        breathing.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (hand == false && handOver == true)
        {
            if(applyProcess == false)
            {
                eButton.GetComponent<SpriteRenderer>().enabled = true;
            }
            if (Input.GetKey("e"))
            {

                if (applyProcess == false)
                {
                    eButton.GetComponent<SpriteRenderer>().enabled = false;
                    applyTimer = 1.0f;
                    applyProcess = true;
                }
                Loadbar.SetActive(true);
                applyTimer -= Time.deltaTime;
                applyBar.localScale = new Vector3(applyTimer, 1f);
                if (applyTimer <= 0)
                {
                    Debug.Log("CompleteHand");
                    hand = true;
                    applyTimer = 1.0f;
                    Loadbar.SetActive(false);
                    applyProcess = false;
                }
            }
            else if (Input.GetKeyUp("e") || handOver == false)
            {
                Loadbar.SetActive(false);
                applyProcess = false;
                applyTimer = 1.0f;
            }
        }
        else if(handOver == false && hand == false)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }



        if (bandaged == false && bandageOver == true && hand == true && ResourceCount.bandageCount > 0)
        {
            if(applyProcess == false)
            {
                eButton.GetComponent<SpriteRenderer>().enabled = true;
            }
            if (Input.GetKey("e"))
            {
                if (applyProcess == false)
                {
                    eButton.GetComponent<SpriteRenderer>().enabled = false;
                    applyTimer = 2.0f;
                    applyProcess = true;
                }
                Loadbar.SetActive(true);
                applyTimer -= Time.deltaTime;
                applyBar.localScale = new Vector3(applyTimer / 2.0f, 1f);
                if (applyTimer <= 0)
                {
                    tape.Play();
                    ResourceCount.bandageCount -= 1;
                    Debug.Log("Bandages Left:" + ResourceCount.bandageCount);
                    Loadbar.SetActive(false);
                    bandaged = true;
                    applyTimer = 1.0f;
                    applyProcess = false;
                }
            }
            else if (Input.GetKeyUp("e") || bandageOver == false)
            {
                applyTimer = 1.0f;
                applyProcess = false;
                Loadbar.SetActive(false);
            }
        }
        else if(bandageOver == false && bandaged == false && hand == true)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }

        if (water == false && waterOver == true && bandaged == true && hand == true && ResourceCount.waterCount > 0)
        {
            if(applyProcess == false)
            {
                eButton.GetComponent<SpriteRenderer>().enabled = true;
            }
            if (Input.GetKey("e"))
            {
                if (applyProcess == false)
                {
                    eButton.GetComponent<SpriteRenderer>().enabled = false;
                    applyTimer = 1.5f;
                    applyProcess = true;
                }
                Loadbar.SetActive(true);
                applyTimer -= Time.deltaTime;
                applyBar.localScale = new Vector3(applyTimer / 1.5f, 1f);
                if (applyTimer <= 0)
                {

                    ResourceCount.waterCount -= 1;
                    Debug.Log("Water Left: " + ResourceCount.waterCount);
                    breathing.Stop();
                    current.SetActive(false);
                    change.SetActive(true);
                    Loadbar.SetActive(false);
                    water = true;
                    applyTimer = 1.0f;
                    applyProcess = false;
                }
            }
            else if (Input.GetKeyUp("e") || waterOver == false)
            {
                applyTimer = 1.0f;
                Loadbar.SetActive(false);
                applyProcess = false;
            }
        }
        else if(waterOver == false && water == false && hand == true && bandaged == true)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }

        if (bandaged == true && hand == true && water == true && switches == false)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
            H4.GetComponent<HealthBar4>().head = true;
            switches = true;
            //set Treated == true;
            //set Done == true;
        }

        if (applyProcess == true)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }
    }
}
