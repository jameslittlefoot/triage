﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegSnap : MonoBehaviour
{
    private bool hand;

    private float applyTimer = 3.0f;

    public bool handOver = false;
    public GameObject change;
    public GameObject H2;
    public GameObject target;
    public GameObject Loadbar;
    public GameObject eButton;
    public Transform applyBar;

    public AudioSource snap;
    public AudioSource scream;

    private bool applyProcess = false;

    // Start is called before the first frame update
    void Start()
    {
        hand = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (hand == false && handOver == true)
        {
            if(applyProcess == false)
            {
                eButton.GetComponent<SpriteRenderer>().enabled = true;
            }
            if (Input.GetKey("e"))
            {
                if(applyProcess == false)
                {
                    eButton.GetComponent<SpriteRenderer>().enabled = false;
                    applyProcess = true;
                }
                Loadbar.SetActive(true);
                applyTimer -= Time.deltaTime;
                applyBar.localScale = new Vector3(applyTimer / 3.0f, 1f);

                if (applyTimer <= 0)
                {
                    applyTimer = 1.0f;
                    snap.Play();
                    scream.Play();
                    applyProcess = false;
                    hand = true;
                    change.SetActive(true);
                    target.SetActive(false);
                    Loadbar.SetActive(false);
                }
            }
            else if (Input.GetKeyUp("e") || handOver == false)
            {
                applyTimer = 3.0f;
                Loadbar.SetActive(false);
                applyProcess = false;
            }
        }
        else if (handOver == false && hand == false)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }

        if (applyProcess == true)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }


    }
}
