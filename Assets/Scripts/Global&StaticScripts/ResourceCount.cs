﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResourceCount : MonoBehaviour
{
    public static int bandageCount = 5;
    public static int waterCount = 5;
    public static int alcCount = 5;
    public static int splintCount = 5;
    public static int burngelCount = 5;
    public static int threadCount = 5;
    public static int morphineCount = 5;

    //public GameObject bandageNum;
    //public GameObject waterNum;
    //public GameObject alcNum;
    //public GameObject splintNum;
    //public GameObject burngelNum;
    //public GameObject threadNum;
    //public GameObject morphineNum;


    //Status of each of the patients, true indicates that they are still alive, false indicates they are dead
    public static bool Patient1Status = true;
    public static bool Patient2Status = true;
    public static bool Patient3Status = true;
    public static bool Patient4Status = true;

    //Status of each treatment, false indicates that the patient can still be worked on, true states that they cannot because they are either completed or dead.
    public bool Patient1Treatment = false;
    public bool Patient2Treatment = false;
    public bool Patient3Treatment = false;
    public bool Patient4Treatment = false;

    private bool flipflop = false;

    public bool[] Patients;
    public bool[] statusList;
    public GameObject H1;
    public GameObject H2;
    public GameObject H3;
    public GameObject H4;
    public GameObject HandBookCanvas;
    public AudioSource danger;
    private Camera camCurrent;

    public Camera cam1;
    public Camera cam2;
    public Camera cam3;
    public Camera cam4;

    public GameObject Canvas1;
    public GameObject Canvas2;
    public GameObject Canvas3;
    public GameObject Canvas4;

    public float newHealth;
    public float totalHealth;
    public float barHealth;

    public bool dangerSound = false;

    // Start is called before the first frame update

    public bool update = false;
    public GameObject FadeIn;
    private int index;
    void Start()
    {
        Patients = new bool[4];
        statusList = new bool[4];

        Patients[0] = Patient1Treatment;
        Patients[1] = Patient2Treatment;
        Patients[2] = Patient3Treatment;
        Patients[3] = Patient4Treatment;

        statusList[0] = Patient1Status;
        statusList[1] = Patient2Status;
        statusList[2] = Patient3Status;
        statusList[3] = Patient4Status;
    }

    // Update is called once per frame
    void Update()
    {
        if(!danger.isPlaying)
        {
            dangerSound = false;
        }
        
        if (update == true)
        {
            Patients[0] = Patient1Treatment;
            Patients[1] = Patient2Treatment;
            Patients[2] = Patient3Treatment;
            Patients[3] = Patient4Treatment;

            statusList[0] = Patient1Status;
            statusList[1] = Patient2Status;
            statusList[2] = Patient3Status;
            statusList[3] = Patient4Status;
            update = false;
        }
        if (Patient1Treatment == true && Patient2Treatment == true && Patient3Treatment == true && Patient4Treatment == true && flipflop == false)
        {
            Debug.Log("check");
            FadeIn.GetComponent<FadeInPatientScript>().startFading();
            flipflop = true;
            Debug.Log("CompleteDay");
            Invoke("switchGameOver", 5f);
        }
        /*bandageNum.GetComponent<Text>().text = " x " + bandageCount.ToString();
        waterNum.GetComponent<Text>().text = " x " + waterCount.ToString();
        alcNum.GetComponent<Text>().text = " x " + alcCount.ToString();
        threadNum.GetComponent<Text>().text = " x " + threadCount.ToString();
        splintNum.GetComponent<Text>().text = " x " + splintCount.ToString();
        burngelNum.GetComponent<Text>().text = " x " + burngelCount.ToString();
        morphineNum.GetComponent<Text>().text = " x " + morphineCount.ToString();*/
        camCurrent = Camera.main;
        HandBookCanvas.GetComponent<Canvas>().worldCamera = camCurrent;

        if (camCurrent == cam1)
        {
            index = 0;
             totalHealth = H1.GetComponent<HealthBar>().totalHealth;
             newHealth = H1.GetComponent<HealthBar>().newHealth;
             barHealth = newHealth / totalHealth;

            if (barHealth <= .4f && dangerSound == false && !danger.isPlaying)
            {
                Debug.Log("check");
                danger.Play();
                dangerSound = true;
            }
            if (barHealth <= 0)
            {
                danger.Pause();
                dangerSound = false;
                switchCamera(index);
            }
        }
        if (camCurrent == cam2)
        {
            index = 1;
             totalHealth = H2.GetComponent<HealthBar2>().totalHealth;
             newHealth = H2.GetComponent<HealthBar2>().newHealth;
             barHealth = newHealth / totalHealth;

            if (barHealth <= .4f && dangerSound == false && !danger.isPlaying)
            {
                Debug.Log("check");
                danger.Play();
                dangerSound = true;
            }
            if (barHealth <= 0)
            {
                danger.Pause();
                dangerSound = false;
                switchCamera(index);
            }
        }
        if (camCurrent == cam3)
        {
            index = 2;
             totalHealth = H3.GetComponent<HealthBar3>().totalHealth;
             newHealth = H3.GetComponent<HealthBar3>().newHealth;
             barHealth = newHealth / totalHealth;

            if (barHealth <= .4f && dangerSound == false && !danger.isPlaying)
            {
                Debug.Log("check");
                danger.Play();
                dangerSound = true;
            }
            if (barHealth <= 0)
            {
                danger.Pause();
                dangerSound = false;
                switchCamera(index);
            }
        }
        if (camCurrent == cam4)
        {
            index = 3;
             totalHealth = H4.GetComponent<HealthBar4>().totalHealth;
             newHealth = H4.GetComponent<HealthBar4>().newHealth;
             barHealth = newHealth / totalHealth;

            if (barHealth <= .4f && dangerSound == false && !danger.isPlaying)
            {
                danger.Play();
                dangerSound = true;
            }
            if (barHealth <= 0)
            {
                danger.Pause();
                dangerSound = false;
                switchCamera(index);
            }
        }

    }
    void switchGameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    // If a patient dies while the player is working on the patient this function will direct them to the next not dead patient
    void switchCamera(int index)
    {
        var check = false;
        var i = index + 1;
        if(i>3)
        {
            i = 0;
        }
        while(check == false)
        {
            if (statusList[i] != true)
            {
                if (i > 3)
                {
                    i = 0;
                }
                else
                {
                    i++;
                }
            }
            else if (statusList[i] == true)
            {
                if(i == 0)
                {
                    cam1.gameObject.SetActive(true);
                    cam2.gameObject.SetActive(false);
                    cam3.gameObject.SetActive(false);
                    cam4.gameObject.SetActive(false);

                    Canvas1.SetActive(true);
                    Canvas2.SetActive(false);
                    Canvas3.SetActive(false);
                    Canvas4.SetActive(false);
                }

                if (i == 1)
                {
                    cam1.gameObject.SetActive(false);
                    cam2.gameObject.SetActive(true);
                    cam3.gameObject.SetActive(false);
                    cam4.gameObject.SetActive(false);

                    Canvas1.SetActive(false);
                    Canvas2.SetActive(true);
                    Canvas3.SetActive(false);
                    Canvas4.SetActive(false);
                }

                if (i == 2)
                {
                    cam1.gameObject.SetActive(false);
                    cam2.gameObject.SetActive(false);
                    cam3.gameObject.SetActive(true);
                    cam4.gameObject.SetActive(false);

                    Canvas1.SetActive(false);
                    Canvas2.SetActive(false);
                    Canvas3.SetActive(true);
                    Canvas4.SetActive(false);
                }

                if (i == 3)
                {
                    cam1.gameObject.SetActive(false);
                    cam2.gameObject.SetActive(false);
                    cam3.gameObject.SetActive(false);
                    cam4.gameObject.SetActive(true);

                    Canvas1.SetActive(false);
                    Canvas2.SetActive(false);
                    Canvas3.SetActive(false);
                    Canvas4.SetActive(true);
                }
                check = true;
            }
        }
    }
}
