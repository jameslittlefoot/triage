﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BandageCollision : MonoBehaviour
{
    public GameObject bandages;
    private void Start()
    {
        bandages = GameObject.FindGameObjectWithTag("bandageUI");
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag == "giveMeds")
        {
            Physics2D.IgnoreCollision(bandages.GetComponent<Collider2D>(), bandages.GetComponent<Collider2D>());
        }
        if (col.gameObject.tag == "giveBandages")
        {
            Debug.Log("Collision");
            bandages.GetComponent<BandageCount>().isOn = true;
        }      
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "giveMeds")
        {
            Physics2D.IgnoreCollision(bandages.GetComponent<Collider2D>(), bandages.GetComponent<Collider2D>());
        }
        if (col.gameObject.tag == "giveBandages")
        {
            Debug.Log("Exit");
            bandages.GetComponent<BandageCount>().isOn = false;
        }
    }
}
