﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionWater : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Concussion;

    private string concussionString;
    void Start()
    {
        concussionString = Concussion.gameObject.tag;
        //Concussion = GameObject.FindGameObjectWithTag("SnappedLeg");
    }
    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.tag != concussionString)
        {
            Physics2D.IgnoreCollision(Concussion.GetComponent<Collider2D>(), Concussion.GetComponent<Collider2D>());
        }
        if (col.gameObject.tag == concussionString)
        {
            Debug.Log("Collision");
            Concussion.GetComponent<Concussion>().waterOver = true;
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag != concussionString)
        {
            Physics2D.IgnoreCollision(Concussion.GetComponent<Collider2D>(), Concussion.GetComponent<Collider2D>());
        }
        if (col.gameObject.tag == concussionString)
        {
            Debug.Log("Collision");
            Concussion.GetComponent<Concussion>().waterOver = false;
        }

    }
}
