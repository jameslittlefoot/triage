﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class LettersScript : MonoBehaviour
{
    public GameObject Erika;
    public GameObject Jack;
    public GameObject Ryan;
    public GameObject Martin;
    public SpriteRenderer Letter1;
    public SpriteRenderer Letter2;
    public SpriteRenderer Letter3;
    public SpriteRenderer Letter4;

    public int CurrentLetter = 0;
    public int LetterTotal = 0;
    public List<GameObject> letters = new List<GameObject>();

    void Start()
    {
        Erika = GameObject.Find("ErikaTemp");
        Jack = GameObject.Find("JackTemp");
        Ryan = GameObject.Find("RyanTemp");
        Martin = GameObject.Find("MartinTemp");


        Letter1 = Martin.GetComponent<SpriteRenderer>();
        Letter2 = Erika.GetComponent<SpriteRenderer>();
        Letter3 = Jack.GetComponent<SpriteRenderer>();
        Letter4 = Ryan.GetComponent<SpriteRenderer>();


        //if Martin dead show martin letter TEMP POSITIONS IT WONT ACTUALLY BE LIKE THIS WHEN THE ACTUAL LETTER ASSET IS DONE
        if (ResourceCount.Patient1Status == false)
        {
            letters.Add(Martin);
            LetterTotal++;
            //Martin.transform.localPosition = new Vector3(0f, 0f, 0f);
        }
        //if erika dead show erika letter TEMP POSITIONS IT WONT ACTUALLY BE LIKE THIS WHEN THE ACTUAL LETTER ASSET IS DONE
        if (ResourceCount.Patient2Status == false)
        {
            letters.Add(Erika);
            LetterTotal++;
            //Erika.transform.localPosition = new Vector3(0f, 0f, 0f);
        }
       //if jack dead show jack letter TEMP POSITIONS IT WONT ACTUALLY BE LIKE THIS WHEN THE ACTUAL LETTER ASSET IS DONE
        if (ResourceCount.Patient3Status == false)
        {
            letters.Add(Jack);
            LetterTotal++;
            // Jack.transform.localPosition = new Vector3(0, 0f, 0f);
        }
        //if ryan dead show ryan letter TEMP POSITIONS IT WONT ACTUALLY BE LIKE THIS WHEN THE ACTUAL LETTER ASSET IS DONE
        if (ResourceCount.Patient4Status == false)
        {
            letters.Add(Ryan);
            LetterTotal++;
            // Ryan.transform.localPosition = new Vector3(0f, 0f, 0f);
        }


    }

    public void NextLetter()
    {
        CurrentLetter++; //increment letter
    }
    public void PrevLetter()
    {
        CurrentLetter--; //decrement letter
    }
    // Update is called once per frame
    void Update()
    {
        if(CurrentLetter > LetterTotal)
        {
            CurrentLetter = 0;
        }
        if(CurrentLetter < 0)
        {
            CurrentLetter = LetterTotal;
        }

        if(letters[CurrentLetter] == Martin)
        {
            Martin.transform.localPosition = new Vector3(0f, 0f, 0f);
            Letter1.color = new Color(1f, 1f, 1f, 1f);
            Letter2.color = new Color(1f, 1f, 1f, 0f);
            Letter3.color = new Color(1f, 1f, 1f, 0f);
            Letter4.color = new Color(1f, 1f, 1f, 0f);
        }

        if(letters[CurrentLetter] == Erika)
        {
            Erika.transform.localPosition = new Vector3(0f, 0f, 0f);
            Letter1.color = new Color(1f, 1f, 1f, 0f);
            Letter2.color = new Color(1f, 1f, 1f, 1f);
            Letter3.color = new Color(1f, 1f, 1f, 0f);
            Letter4.color = new Color(1f, 1f, 1f, 0f);
        }

        if(letters[CurrentLetter] == Jack)
        {
            Jack.transform.localPosition = new Vector3(0f, 0f, 0f);
            Letter1.color = new Color(1f, 1f, 1f, 0f);
            Letter2.color = new Color(1f, 1f, 1f, 0f);
            Letter3.color = new Color(1f, 1f, 1f, 1f);
            Letter4.color = new Color(1f, 1f, 1f, 0f);
        }

        if (letters[CurrentLetter] == Ryan)
        {
            Ryan.transform.localPosition = new Vector3(0f, 0f, 0f);
            Letter1.color = new Color(1f, 1f, 1f, 0f);
            Letter2.color = new Color(1f, 1f, 1f, 0f);
            Letter3.color = new Color(1f, 1f, 1f, 0f);
            Letter4.color = new Color(1f, 1f, 1f, 1f);
        }
    }
}

