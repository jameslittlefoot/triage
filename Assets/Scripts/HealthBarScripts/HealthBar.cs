﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private Transform bar;

    public float totalHealth;
    public float newHealth;
    private float barHealth;
    public bool treated = false;
    public bool done = false;
    public GameObject global;
    public AudioSource dead;
    public AudioSource lowhealth;

    public GameObject completeMark;
    public GameObject completeMark1;
    public GameObject completeMark2;
    public GameObject completeMark3;

    public GameObject DisableIcon1;
    public GameObject DisableIcon2;
    public GameObject DisableIcon3;
    public GameObject DisableIcon4;

    public GameObject FadeIn1;

    public AudioSource warning;
    private bool warned = false;
    private bool flipflop = false;
    public AudioSource trouble;
    private bool danger = false;
    // Start is called before the first frame update
    void Start()
    {
        //InvokeRepeating("Count", 0.0f, 1.0f);
        bar = transform.Find("Bar");
        bar.localScale = new Vector3(totalHealth, 1f);
        dead = GetComponent<AudioSource>();

        //healthdecrease = .001f;

    }
    /* public void healfunction()
     {
         if (newHealth < 1f && newHealth > 0)
         {
             newHealth += .1f;
         }


     }
     */

    /*void Count()
    {
        if(done == false)
        {
            newHealth = newHealth - 1.0f;
            barHealth = newHealth / totalHealth;
            bar.localScale = new Vector3(newHealth / totalHealth, 1f);
            if(newHealth < 10f)
            {
                lowhealth.Play();
            }
        }
        else
        {
            CancelInvoke("Count");
        }

    }*/

    public void SetColor(Color color)
    {
        var fillbar = bar.Find("BarFill").GetComponent<SpriteRenderer>().color = color;
    }
    // Update is called once per frame
    void Update()
    {
        Color orange = new Color(.9f, 0.5f, 0.0f);
        Color yellowgreen = new Color(.3f, 0.9f, 0.0f);

        barHealth = newHealth / totalHealth;


        //Checks if the health decrease module is negative so that the player can't be stabilizing the patient to the point where it heals them.
        if (newHealth >= 0 && done == false)
        {
            newHealth -= Time.deltaTime;
            barHealth = newHealth / totalHealth;
            bar.localScale = new Vector3(barHealth, 1f);
        }

        //checks if dead and plays sound
        /*if (newHealth <= 0 && gameover == false)
        {
            dead.Play();
            gameover = true;
            Invoke("switchGameOver", 5f);
        }*/

        //changes health color based on value
        if (barHealth < .2f)
        {
            bar.Find("BarFill").GetComponent<SpriteRenderer>().color = Color.red;
        }
        else if (barHealth > .4f && barHealth < .6f)
        {
            
            bar.Find("BarFill").GetComponent<SpriteRenderer>().color = Color.yellow;
        }
        else if (barHealth > .2f && barHealth < .4f)
        {
            bar.Find("BarFill").GetComponent<SpriteRenderer>().color = orange;
        }
        else if (barHealth > .6f && barHealth < .8f)
        {
            bar.Find("BarFill").GetComponent<SpriteRenderer>().color = yellowgreen;
        }
        else if (barHealth > .8f)
        {
            bar.Find("BarFill").GetComponent<SpriteRenderer>().color = Color.green;
        }

        if (newHealth < 20f && warned == false)
        {
            warning.Play();
            warned = true;
        }


        if (newHealth <= 0 && done == false)
        {
            done = true;
            Debug.Log("P1 Dead");
            trouble.Stop();
            global.GetComponent<ResourceCount>().Patient1Treatment = true;
            ResourceCount.Patient1Status = false;
            global.GetComponent<ResourceCount>().update = true;
        }
        if(done == true && treated == true && flipflop == false)
        {
            flipflop = true;
            warning.Stop();
            Debug.Log("P1 treated");
            completeMark.SetActive(true);
            completeMark1.SetActive(true);
            completeMark2.SetActive(true);
            completeMark3.SetActive(true);


            DisableIcon1.GetComponent<Button>().enabled = false;
            DisableIcon2.GetComponent<Button>().enabled = false;
            DisableIcon3.GetComponent<Button>().enabled = false;
            DisableIcon4.GetComponent<Button>().enabled = false;
            global.GetComponent<ResourceCount>().Patient1Treatment = true;
            global.GetComponent<ResourceCount>().update = true;
        }
    }

    void switchGameOver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

}
