﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthBar5 : MonoBehaviour
{

    private Transform bar5;

    private float totalHealth = 1f;
    public float newHealth = 1f;
    public float healthdecrease;

    // Use this for initialization
    void Start()
    {
        bar5 = transform.Find("Bar5");
        bar5.localScale = new Vector3(totalHealth, 1f);
        healthdecrease = .00075f; //can be adjusted later
    }



    public void healfunction()
    {
        if (newHealth < 1f && newHealth > 0)
        {
            newHealth += .1f;
        }


    }

    // Update is called once per frame
    void Update()
    {

        // stops health from going negative and also makes sure it is constantly depleting.
        if (newHealth > 0)
        {
            newHealth = newHealth - healthdecrease;



            bar5.localScale = new Vector3(newHealth, 1f);
        }
        //Prevents the bar from overfilling aka overheal
        if (newHealth > 1f)
        {
            newHealth = 1f;
        }

        if (healthdecrease <= 0)
        {
            healthdecrease = 0.0001f;
        }
    }
}

