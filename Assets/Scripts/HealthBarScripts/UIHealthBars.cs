﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHealthBars : MonoBehaviour
{
    public GameObject Primary;
    public Transform Clone;
    public float barHealth;
    public float totalHealth;
    public float newHealth;
    public GameObject barFill;
    private string barFillName;
    // Start is called before the first frame update
    void Start()
    {
        barFillName = barFill.gameObject.name;
    }

    // Update is called once per frame
    void Update()
    {
        Color orange = new Color(.9f, 0.5f, 0.0f);
        Color yellowgreen = new Color(.3f, 0.9f, 0.0f);
        Clone.localScale = new Vector3(totalHealth, 1f);

        if (Primary.gameObject.name == "HealthBar")
        {
            totalHealth = Primary.GetComponent<HealthBar>().totalHealth;
            newHealth = Primary.GetComponent<HealthBar>().newHealth;
            barHealth = newHealth / totalHealth;
            Clone.localScale = new Vector3(barHealth, 1f);
        }
        else if(Primary.gameObject.name == "HealthBar2")
        {
            totalHealth = Primary.GetComponent<HealthBar2>().totalHealth;
            newHealth = Primary.GetComponent<HealthBar2>().newHealth;
            barHealth = newHealth / totalHealth;
            Clone.localScale = new Vector3(barHealth, 1f);
        }
        else if (Primary.gameObject.name == "HealthBar3")
        {
            totalHealth = Primary.GetComponent<HealthBar3>().totalHealth;
            newHealth = Primary.GetComponent<HealthBar3>().newHealth;
            barHealth = newHealth / totalHealth;
            Clone.localScale = new Vector3(barHealth, 1f);
        }
        else if (Primary.gameObject.name == "HealthBar4")
        {
            totalHealth = Primary.GetComponent<HealthBar4>().totalHealth;
            newHealth = Primary.GetComponent<HealthBar4>().newHealth;
            barHealth = newHealth / totalHealth;
            Clone.localScale = new Vector3(barHealth, 1f);
        }

        if (barHealth < .2f)
        {
            Clone.Find(barFillName).GetComponent<SpriteRenderer>().color = Color.red;
        }
        else if (barHealth > .4f && barHealth < .6f)
        {
            Clone.Find(barFillName).GetComponent<SpriteRenderer>().color = Color.yellow;
        }
        else if (barHealth > .2f && barHealth < .4f)
        {
            Clone.Find(barFillName).GetComponent<SpriteRenderer>().color = orange;
        }
        else if (barHealth > .6f && barHealth < .8f)
        {
            Clone.Find(barFillName).GetComponent<SpriteRenderer>().color = yellowgreen;
        }
        else if (barHealth > .8f)
        {
            Clone.Find(barFillName).GetComponent<SpriteRenderer>().color = Color.green;
        }
    }
}
