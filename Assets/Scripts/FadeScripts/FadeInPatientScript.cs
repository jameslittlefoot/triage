﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FadeInPatientScript : MonoBehaviour
{
    public Image blackFade;
    public TextMeshProUGUI text1;
    public TextMeshProUGUI text2;
    public TextMeshProUGUI text3;
    public TextMeshProUGUI text4;
    // Start is called before the first frame update
    void Start()
    {
        blackFade.canvasRenderer.SetAlpha(0.0f);
        //startFading();
    }

    // Update is called once per frame


    public void startFading()
    {
        Debug.Log("FadeGo");
        blackFade.CrossFadeAlpha(1, 3, false);
        text1.GetComponent<FadeInTextScript>().startFading();
        text2.GetComponent<FadeInTextScript>().startFading();
        text3.GetComponent<FadeInTextScript>().startFading();
        text4.GetComponent<FadeInTextScript>().startFading();
    }
}
