﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class FadeInTextScript : MonoBehaviour
{
    public TextMeshProUGUI textFade;
    // Start is called before the first frame update
    void Start()
    {
        textFade.canvasRenderer.SetAlpha(0.0f);
        //startFading();
    }

    // Update is called once per frame


    public void startFading()
    {       
        textFade.CrossFadeAlpha(1, 3, false);
    }
}
