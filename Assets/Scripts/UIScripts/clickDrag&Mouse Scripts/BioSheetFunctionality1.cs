﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BioSheetFunctionality1 : MonoBehaviour
{

    public GameObject Char1;
    public GameObject Char2;
    public GameObject Char3;
    public GameObject Char4;
    public SpriteRenderer BioSheet1;
    public SpriteRenderer BioSheet2;
    public SpriteRenderer BioSheet3;
    public SpriteRenderer BioSheet4;

    public List<GameObject> bioSheets = new List<GameObject>();
    public int currentSheet = 0;
    public AudioSource turn;
    // Start is called before the first frame update
    void Start()
    {
        Char1 = GameObject.Find("Character1");
        Char2 = GameObject.Find("Character2");
        Char3 = GameObject.Find("Character3");
        Char4 = GameObject.Find("Character4");
        BioSheet1 = Char1.GetComponent<SpriteRenderer>();
        BioSheet2 = Char2.GetComponent<SpriteRenderer>();
        BioSheet3 = Char3.GetComponent<SpriteRenderer>();
        BioSheet4 = Char4.GetComponent<SpriteRenderer>();
        bioSheets.Add(Char1);
        bioSheets.Add(Char2);
        bioSheets.Add(Char3);
        bioSheets.Add(Char4);
    }

    public void NextSheet()
    {
        currentSheet++; //increment biosheets
        turn.Play();
    }

    public void PrevSheet()
    {
        currentSheet--; //decrement biosheets
        turn.Play();
    }
    // Update is called once per frame
    void Update()
    {


        if(currentSheet > 3)
        {
            currentSheet = 0;
        }

        if(currentSheet < 0)
        {
            currentSheet = 3;
        }

        if(bioSheets[currentSheet] == Char1)
        {
            BioSheet1.color = new Color(1f, 1f, 1f, 1f);
            BioSheet2.color = new Color(1f, 1f, 1f, 0f);
            BioSheet3.color = new Color(1f, 1f, 1f, 0f);
            BioSheet4.color = new Color(1f, 1f, 1f, 0f);
        }
        if(bioSheets[currentSheet] == Char2)
        {
            BioSheet1.color = new Color(1f, 1f, 1f, 0f);
            BioSheet2.color = new Color(1f, 1f, 1f, 1f);
            BioSheet3.color = new Color(1f, 1f, 1f, 0f);
            BioSheet4.color = new Color(1f, 1f, 1f, 0f);
        }
        if (bioSheets[currentSheet] == Char3)
        {
            BioSheet1.color = new Color(1f, 1f, 1f, 0f);
            BioSheet2.color = new Color(1f, 1f, 1f, 0f);
            BioSheet3.color = new Color(1f, 1f, 1f, 1f);
            BioSheet4.color = new Color(1f, 1f, 1f, 0f);
        }
        if (bioSheets[currentSheet] == Char4)
        {
            BioSheet1.color = new Color(1f, 1f, 1f, 0f);
            BioSheet2.color = new Color(1f, 1f, 1f, 0f);
            BioSheet3.color = new Color(1f, 1f, 1f, 0f);
            BioSheet4.color = new Color(1f, 1f, 1f, 1f);
        }
        // Char2.transform.position = new Vector3(Char1.transform.position.x + 2, Char1.transform.position.y + 3, 1);
    }
}
