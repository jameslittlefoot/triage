﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickDragManual : MonoBehaviour
{
    private float startPosX;
    private float startPosY;
    private float newPosX;
    private float newPosY;
    private bool hold = false;

    // Update is called once per frame
    void Update()
    {
        // What happens when the sprite is hold down.
        if (hold == true)
        {
            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);
            newPosX = mousePos.x - startPosX;
            newPosY = mousePos.y - startPosY;
            newPosX = Mathf.Clamp(newPosX, -1250, 1250);
            newPosY = Mathf.Clamp(newPosY, -1000, 1000);
            transform.localPosition = new Vector3(newPosX, newPosY, -5);
        }
    }

    private void OnMouseDown()
    {
        // What happens at the very moment the sprite is clicked on.
        if(Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);

            startPosX = mousePos.x - transform.localPosition.x;
            startPosY = mousePos.y - transform.localPosition.y;
            hold = true;
        }
    }

    private void OnMouseUp()
    {
        // What happens at the very moment the sprite is let go of.
        hold = false;
    }
}
