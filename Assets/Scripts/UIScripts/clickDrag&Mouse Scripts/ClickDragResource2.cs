﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickDragResource2 : MonoBehaviour
{
    private float startPosX;
    private float startPosY;
    private bool hold = false;
    private float startLocationX;
    private float startLocationY;
    private Text bandageCount;
    private Text pillCount;



    private void Start()
    {
        startLocationX = this.gameObject.transform.localPosition.x;
        startLocationY = this.gameObject.transform.localPosition.y;
    }

    private float newPosX;
    private float newPosY;



    // Update is called once per frame
    void Update()
    {
        // What happens when the sprite is hold down.
        if (hold == true)
        {
            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);


            this.gameObject.transform.localPosition = new Vector3(mousePos.x - startPosX, mousePos.y - startPosY, 90);


            newPosX = mousePos.x - startPosX;
            newPosY = mousePos.y - startPosY;
            newPosX = Mathf.Clamp(newPosX, -1250, 1250);
            newPosY = Mathf.Clamp(newPosY, -1000, 1000);
            transform.localPosition = new Vector3(newPosX, newPosY, 90);

        }
    }

    private void OnMouseDown()
    {
        // What happens at the very moment the sprite is clicked on.

        if (Input.GetMouseButtonDown(0))

            if (Input.GetMouseButtonDown(0))

            {
                Vector3 mousePos;
                mousePos = Input.mousePosition;
                mousePos = Camera.main.ScreenToWorldPoint(mousePos);


                startPosX = mousePos.x - this.transform.localPosition.x;
                startPosY = mousePos.y - this.transform.localPosition.y;

                hold = true;
            }
    }


    private void OnMouseUp() //will snap back to the table if not applied to the wound, wound part will be added later for now it just snaps back
    {
        // What happens at the very moment the sprite is let go of.
        if (Input.GetMouseButtonUp(0))
        {
            this.gameObject.transform.localPosition = new Vector3(startLocationX, startLocationY, 90);
            hold = false;
        }

        /*if(this.gameObject.transform.localPosition.x != startLocationX && this.gameObject.transform.localPosition.y != startLocationY)
        {
            this.gameObject.transform.localPosition = new Vector3(startLocationX, startLocationY, 0);
        } */
    }
}
