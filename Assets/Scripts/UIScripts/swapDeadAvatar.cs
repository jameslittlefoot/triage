﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class swapDeadAvatar : MonoBehaviour
{
    public GameObject healthObject;
    private float newHealth;
    public GameObject current;
    public GameObject change;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {


        if (healthObject.gameObject.name == "HealthBar")
        {
            newHealth = healthObject.GetComponent<HealthBar>().newHealth;
            if(newHealth <= 0)
            {
                current.SetActive(false);
                change.SetActive(true);
            }
        }
        else if (healthObject.gameObject.name == "HealthBar2")
        {
            newHealth = healthObject.GetComponent<HealthBar2>().newHealth;
            if (newHealth <= 0)
            {
                current.SetActive(false);
                change.SetActive(true);
            }
        }
        else if (healthObject.gameObject.name == "HealthBar3")
        {
            newHealth = healthObject.GetComponent<HealthBar3>().newHealth;
            if (newHealth <= 0)
            {
                current.SetActive(false);
                change.SetActive(true);
            }
        }
        else if (healthObject.gameObject.name == "HealthBar4")
        {
            newHealth = healthObject.GetComponent<HealthBar4>().newHealth;
            if (newHealth <= 0)
            {
                current.SetActive(false);
                change.SetActive(true);
            }
        }
    }
}
