﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCamera : MonoBehaviour
{
    public Camera cam1;
    public Camera cam2;
    // Start is called before the first frame update
    

     void OnMouseDown()
    {
        cam1.gameObject.SetActive(false);
        cam2.gameObject.SetActive(true);
    }
    // Update is called once per frame
}
