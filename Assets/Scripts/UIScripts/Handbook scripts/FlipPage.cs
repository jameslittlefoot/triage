﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlipPage : MonoBehaviour
{
    private Handbook handbook; // Make a handbook to use.

    // Start is called before the first frame update
    void Start()
    {
        handbook = GameObject.FindObjectOfType<Handbook>(); // Find the handbook in the scene.
    }

    public void OnButtonPress()
    {
        if (this.name == "LeftPage") // If the button on the left page is clicked on
        {
            if (handbook.currentPage != 1) // If the handbook is not on the first page
            {
                handbook.currentPage -= 1; // Go to the previous page and change the sprite accordingly
                handbook.GetComponent<SpriteRenderer>().sprite = handbook.bookPages[handbook.currentPage];
            }
        }
        else if (this.name == "RightPage") // If the button on the right page is clicked on
        {
            if (handbook.currentPage != handbook.maxPages) // If the handbook is not on the last page
            {
                handbook.currentPage += 1; // Go to the next page and change the sprite accordingly
                handbook.GetComponent<SpriteRenderer>().sprite = handbook.bookPages[handbook.currentPage];
            }
        }
    }
}