﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class resourceNumScript : MonoBehaviour
{
    public Text textTarget;

    private string textName;
    // Start is called before the first frame update
    void Start()
    {
        textName = textTarget.gameObject.name;
    }

    // Update is called once per frame
    void Update()
    {
        if (textName == "waterNum")
        {
            textTarget.text = " x " + ResourceCount.waterCount.ToString();
        }
        if (textName == "splintNum")
        {
            textTarget.text = " x " + ResourceCount.splintCount.ToString();
        }
        if (textName == "bandageNum")
        {
            textTarget.text = " x " + ResourceCount.bandageCount.ToString();
        }
        if (textName == "alcNum")
        {
            textTarget.text = " x " + ResourceCount.alcCount.ToString();
        }
        if (textName == "burngelNum")
        {
            textTarget.text = " x " + ResourceCount.burngelCount.ToString();
        }
        if (textName == "threadNum")
        {
            textTarget.text = " x " + ResourceCount.threadCount.ToString();
        }
        if (textName == "morphineNum")
        {
            textTarget.text = " x " + ResourceCount.morphineCount.ToString();
        }
    }
}
