﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SwitchScene : MonoBehaviour
{
    //public string name;
    public GameObject fadeIn;
    void Start()
    {
        //name = this.gameObject.name;
    }
    public void OnButtonPress()
    {
        fadeIn.GetComponent<FadeInGeneral>().startFading();
        if (this.name == "Briefing1FinishButton")
        {
            Invoke("bioSheets", 3f);
        }
        if(this.name == "BeginTreatment")
        {
            Invoke("patientScene", 3f);
        }
        if(this.name == "ContinueButton")
        {
            Invoke("LetterScene", 3f);
        }
        if(this.name == "Global Object")
        {
            Invoke("startMenuScene", 1f);
        }
    }
    void bioSheets()
    {
        SceneManager.LoadScene("BioSheets1");
    }
    void patientScene()
    {
        SceneManager.LoadScene("Patient");
    }
    void gameOverScene()
    {
        SceneManager.LoadScene("Game Over");
    }
    void startMenuScene()
    {
        SceneManager.LoadScene("Start Menu");
    }
    void LetterScene()
    {
        SceneManager.LoadScene("LettersCutScene1");
    }
}

